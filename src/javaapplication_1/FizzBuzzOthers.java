package javaapplication_1;

/**
 *
 * @author Alumne
 */
public class FizzBuzzOthers {

    private boolean isMultiple(int num, int multiple) {
        return num%multiple==0;
    }
	
    public String fizzBuzz(int number) {
        // L'ordre importa !!
        if (isMultiple(number,15)) {
            return "FIZZ BUZZ";
        }
        if (isMultiple(number,3)) {
            return "FIZZ";
        }
        if (isMultiple(number,5)) {
            return "BUZZ";
        }
        return "";
    }
    
}
