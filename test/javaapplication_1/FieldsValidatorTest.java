/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication_1;

import Ej3_FormValidations.Validator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author portatil
 */
public class FieldsValidatorTest {
    
    static Validator myValidator;
    
    public FieldsValidatorTest() {
    }
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        myValidator = new Validator();
    }

    // The methods must be annotated with annotation @Test. For example:
    //
    
    
    @Test
    public void testCorrectEmail() {
        String testEmail = "miquel@gmail.com";
        boolean expectedResult = true;
        boolean result = myValidator.valEmail(testEmail);
        System.out.println("");
        assertEquals(expectedResult, result);
        // 
    }
    
    @Test
    public void testIncorrectEmail1() {
        String testEmail = "Miquel";
        TestFalseEmail(testEmail);
    }
    
    @Test
    public void testIncorrectEmailDotCom() {
        String testEmail = "miquel.com";
        TestFalseEmail(testEmail);
    }

    private void TestFalseEmail(String testEmail) {
        boolean expectedResult = false;
        boolean result = myValidator.valEmail(testEmail);
        System.out.println("");
        assertEquals(expectedResult, result);
        // 
    }
    
    
}
